#include "TRT_ElectronPidTools/BaseTRTPIDCalculator.h"
#include "TRT_ElectronPidTools/TRT_ElectronPidToolRun2.h"
#include "TRT_ElectronPidTools/TRT_LocalOccupancy.h"

DECLARE_COMPONENT( InDet::TRT_ElectronPidToolRun2 )
DECLARE_COMPONENT( InDet::TRT_LocalOccupancy )

